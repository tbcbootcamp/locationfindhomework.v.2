package com.example.locationfindhomework.ui.search

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.locationfindhomework.R
import com.example.locationfindhomework.dataloader.ApiHandler
import com.example.locationfindhomework.dataloader.CustomCallback
import kotlinx.android.synthetic.main.activity_search_location.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.json.JSONObject

class SearchLocationActivity : AppCompatActivity() {

    private lateinit var locationRecyclerViewAdapter: LocationRecyclerViewAdapter
    private var addresses = mutableListOf<LocationModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_location)
        init()
    }

    private fun init() {
        setSupportActionBar(toolbarLayout)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbarTitle.text = getString(R.string.area_of_interest)
        doneButton.text = getString(R.string.add)


        locationRecyclerViewAdapter =
            LocationRecyclerViewAdapter(
                addresses
            )
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = locationRecyclerViewAdapter
        searchEditText.addTextChangedListener(textWatcher)
    }

    private fun getAddresses(input: String) {

        addresses.clear()
        val parameters = mutableMapOf<String, String>()
        parameters["input"] = input
        parameters["key"] = "AIzaSyBukqO846TNjXqU_Mc_kAFFaC3Wdt-PFaU"

        ApiHandler.getRequest(
            ApiHandler.AUTOCOMPLETE,
            parameters,
            object : CustomCallback {

                override fun onFailure(response: String) {
                    Toast.makeText(applicationContext, "failure", Toast.LENGTH_SHORT).show()
                }

                override fun onResponse(response: String) {

                    val json = JSONObject(response)

                    if (json.has("predictions")) {
                        val predictions = json.getJSONArray("predictions")

                        (0 until predictions.length()).forEach {
                            val prediction = predictions.getJSONObject(it)
                            addresses.add(
                                LocationModel(
                                    prediction.getString("description"),
                                    prediction.getString("place_id")
                                )
                            )
                        }
                    }

                    locationRecyclerViewAdapter.notifyDataSetChanged()

                }
            })
    }

    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {

        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            getAddresses(s.toString())
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            super.onBackPressed()
        return true
    }

}