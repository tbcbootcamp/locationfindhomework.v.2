package com.example.locationfindhomework.ui.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.locationfindhomework.R
import kotlinx.android.synthetic.main.item_recyclerview_layout.view.*

class LocationRecyclerViewAdapter(private val items: MutableList<LocationModel>) :
    RecyclerView.Adapter<LocationRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_recyclerview_layout,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        return holder.onBind()
    }

    override fun getItemCount() = items.size

    private lateinit var model: LocationModel

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        fun onBind() {
            model = items[adapterPosition]
            itemView.locationTextView.text = model.description
        }

    }

}