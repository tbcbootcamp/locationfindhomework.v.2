package com.example.locationfindhomework.dataloader

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.QueryMap

object ApiHandler {

    const val AUTOCOMPLETE = "autocomplete"

    private var retrofit = Retrofit.Builder()
        .baseUrl("https://maps.googleapis.com/maps/api/")
        .addConverterFactory(ScalarsConverterFactory.create())
        .build()

    private var service = retrofit.create(
        ApiService::class.java)

    interface ApiService {
        @GET("place/{path}/json")
        fun getRequest(
            @Path("path") path: String,
            @QueryMap parameters: MutableMap<String, String>
        ): Call<String>
    }

    fun getRequest(path: String, parameters: MutableMap<String, String>, callback: CustomCallback) {
        val call = service.getRequest(path, parameters)
        call.enqueue(
            onCallback(
                callback
            )
        )
    }


    private fun onCallback(callback: CustomCallback) = object : Callback<String> {
        override fun onFailure(call: Call<String>, t: Throwable) {
            callback.onFailure(t.message.toString())
        }

        override fun onResponse(call: Call<String>, response: Response<String>) {
            callback.onResponse(response.body().toString())

        }

    }

}