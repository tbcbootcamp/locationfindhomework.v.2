package com.example.locationfindhomework.dataloader

interface CustomCallback {
    fun onResponse(response: String)
    fun onFailure(response: String)
}